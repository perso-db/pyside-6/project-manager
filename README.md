Don Beberto's Project Manager
=============================

The project manager allows launching commonly-used command lines, on a per-project
basis.

Project
-------
The parameters saved for a project are :

- **Working directory** : the path to the project's root
- **Title** : the name of the project in the documentation (PIP, Pyinstaller, Sphinx...)
  .
- **Repo name** : the name of the PIP and conda packages. It has to be the same as the
  name of the sub-folder holding the actual source code.
- **Conda environment** : the name of the conda environment to use to run the project.
- **Version** : the current version. An option is available to update all necessary
  files when changing the version (setup.py, conf.py...)

Commands
--------
The commands are organized by thematic groups, and future versions will allow to edit
them (add / remove). The 6 groups available today are

- Code review
- Sphinx
- Environment
- PIP / Twine
- Conda
- Pyinstaller

Each command can be run either in the default console, or in the project's conda
environment.  
If one needs to execute several commands in the same console (ex: activating a conda
environment before executing another command), commands can be separated by " & ". If
several commands need to be run, but not necessarily in the same console, they can be
separated with "|". Commands can include variable either from the project or from the
config parameters (see Options below), by using the syntax {{project.var_name}} or
{{config.var_name}} respectfully.


Cookiecutter
------------
The gui also allows creating new project or component, using CookieCutter. The path to
the folder holding the cookiecutter templates must be filled in the database under "
cookiecutter_path", and the templates need to be organized in two sub-folders "Projects"
and "Components". If some cookiecutter parameters have the same name as the project, the
one from the current_project will be used in place of the default values from the json
file.

Options
-------

- cookiecutter_path : path to the folder where the cookie cutter templates are stored.
- cookiecutter_working_dir : path to the folder where the Python projects are stored,
  and where the new projects and components will be created.
- pyinstaller_dist_path : path where the pyinstaller bundle will be created.
- conda_script_activation : path to the script to launch to activate conda.

Dependency
----------
The project is using Pyside6 for the GUI. All dependencies can be found in the
project_manager.yml files, and a new conda env can be created by typing  
`conda create -f project_manager_full.yml`  
Once the environment is created, an .exe can be build via  
`pyinstaller donb_project_manager.spec`
