# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="donb-project-manager",
    version="1.1.1",
    author="Don Beberto",
    author_email="bebert64@gmail.com",
    description="",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    package_data={"": ["py.typed"]},
    packages=setuptools.find_packages(where="."),
    install_requires=[
        "Pyside6",
        "donb-custom-widget",
    ],
    extras_require={},
)
