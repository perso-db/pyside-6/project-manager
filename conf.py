# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sys
from pathlib import Path

current_folder = Path(__file__)
doc_folder = current_folder.parent
package_folder = doc_folder.parent
sys.path.insert(0, str(package_folder.absolute()))


# -- Project information -----------------------------------------------------

import datetime

year = datetime.date.today().year
project = "Don Beberto's Project Manager"
copyright = f"{year}, Don Beberto"
author = "Don Beberto"

# The full version, including alpha/beta/rc tags
release = "1.1.1"


# -- General configuration ---------------------------------------------------

# Do not add module names in front of classes
add_module_names = False

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.intersphinx",
]

# Add autodoc options
autodoc_default_options = {
    "members": True,
    "undoc-members": True,
    "member-order": "bysource",
    #'member-order': 'groupwise',
    #'private-members': False,
}
# autodoc_typehints = 'signature'
autodoc_typehints = "description"
# autodoc_typehints = 'none'

# autoclass_content = 'both'
autodoc_docstring_signature = True

autodoc_type_aliases = {}

# Add napoleon options
napoleon_use_ivar = True
napoleon_custom_sections = [
    "Class Attributes",
    "Instance Attributes",
    "Attributes",
    "Methods",
    "Properties",
]
napoleon_use_rtype = True


# Add autodoc_typehints options
# always_document_param_types = True
typehints_fully_qualified = False
set_type_checking_flag = False

# Add intershpinx mappings
intersphinx_mapping = {
    "python": ("http://docs.python.org/3", None),
}


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = "sphinx"


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
html_theme_options = {
    "prev_next_buttons_location": None,
    "collapse_navigation": False,
}


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
