# -*- coding: utf-8 -*-

"""
Defines :
 The MyMainWindow class.

"""

from __future__ import annotations

from pathlib import Path
from typing import List, Tuple, Optional

import peewee
from PySide6 import QtWidgets, QtCore, QtGui
from donb_custom_widget.my_custom_widget import MyCustomWidget
from donb_tools.functions import get_data_folder

from donb_project_manager.config import config
from donb_project_manager.cookiecutter_template import CookiecutterTemplate
from donb_project_manager.models import (
    Project,
    Command,
    Profile,
    ProfileCommand,
)
from donb_project_manager.widgets.command_executioner_widget import (
    CommandExecutionerWidget,
)
from donb_project_manager.widgets.project_manager_widget import ProjectManagerWidget
from donb_project_manager.widgets.select_project_widget import SelectProjectWidget, \
    OpenProjectWidget, RestoreProjectWidget


class MyMainWindow(QtWidgets.QMainWindow, MyCustomWidget):

    """
    MainWindow of the app.

    Warning
    -------
    This widget should not be instantiated directly, but rather through the factory
    method create_my_main_window.

    """

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None):
        super().__init__(parent)
        self.command_check_boxes: List[Tuple[Command, QtWidgets.QCheckBox]] = []

    @classmethod
    def create_my_main_window(cls) -> MyMainWindow:
        """Factory method to create the main window of the app."""
        # create_tab_widget is a factory method, and should therefore be allowed
        # to access protected members of the class.
        # pylint: disable = protected-access
        my_main_window = cls.create_widget()
        assert isinstance(my_main_window, cls)
        my_main_window._setup_ui()
        my_main_window._connect_actions()
        return my_main_window

    def _setup_ui(self) -> None:
        self._setup_edit_project_icon()
        self._add_commands()
        self._add_cookiecutters()
        self.refresh()

    def _setup_edit_project_icon(self) -> None:
        icon_path = get_data_folder() / "images" / "wheel.jpg"
        icon = QtGui.QIcon(str(icon_path.absolute()))
        self.edit_project_button.setIcon(icon)
        self.edit_project_button.setStyleSheet("QPushButton { border: 0px }")

    def _add_commands(self) -> None:
        for command in Command.select():
            assert hasattr(self, command.layout_name)
            layout = getattr(self, command.layout_name)
            check_box = QtWidgets.QCheckBox(self)
            check_box.setText(command.label)
            check_box.toggled.connect(self._save_profile)  # pylint: disable=no-member
            layout.addWidget(check_box)
            self.command_check_boxes.append((command, check_box))

    def _add_cookiecutters(self) -> None:
        cookiecutter_path = config["cookiecutter_dir"]
        for template_path in (cookiecutter_path / "projets").iterdir():
            self._add_cookiecutter(template_path, self.new_project_layout)
        for template_path in (cookiecutter_path / "composants").iterdir():
            self._add_cookiecutter(template_path, self.new_component_layout)

    def _add_cookiecutter(self, template_path: Path, layout: QtWidgets.QLayout) -> None:
        template = CookiecutterTemplate(template_path)
        new_button = QtWidgets.QPushButton(self)
        new_button.template = template
        new_button.setText(new_button.template.name)
        new_button.clicked.connect(  # pylint: disable=no-member
            new_button.template.show_widget
        )
        layout.addWidget(new_button)

    def refresh(self) -> None:
        """Refreshes the title project and the selected commands."""
        try:
            current_project = Project.get(id=config["current_project_id"])
        except peewee.DoesNotExist:
            print("problem")
            print(config["current_project_id"])
            current_project = Project.get()
            config["current_project_id"] = current_project.id
            config.save()
        self.title_label.setText(current_project.title)
        current_profile = Profile.get(id=config["current_profile_id"])
        self._load_profile(current_profile)

    def _connect_actions(self) -> None:
        self.action_new_project.triggered.connect(self._create_new_project)
        self.action_open_project.triggered.connect(self._open_project)
        self.action_restore_project.triggered.connect(self._restore_project)
        self.edit_project_button.clicked.connect(self._edit_current_project)
        self.launch_button.clicked.connect(self._launch_actions)

    def _create_new_project(self) -> None:
        new_project = Project()
        new_project_widget = ProjectManagerWidget.create_project_manager_widget(
            new_project, self
        )
        new_project_widget.setWindowTitle("New Project")
        new_project_widget.show()

    def _open_project(self) -> None:
        open_project_widget = OpenProjectWidget.create_open_project_widget(self)
        open_project_widget.show()

    def _restore_project(self) -> None:
        restore_project_widget = RestoreProjectWidget.create_restore_project_widget(self)
        restore_project_widget.show()

    def _edit_current_project(self) -> None:
        project = Project.get(id=config["current_project_id"])
        project_manager_widget = ProjectManagerWidget.create_project_manager_widget(
            project, self
        )
        project_manager_widget.setWindowTitle(f"Edit {project.title}")
        project_manager_widget.show()

    def _launch_actions(self) -> None:
        commands = self._get_selected_commands()
        # fmt: off
        command_executioner_widget = (
            CommandExecutionerWidget.create_command_executioner_widget(
                commands, self
            )
        )
        # fmt: on
        command_executioner_widget.show()
        command_executioner_widget.execute()

    def _get_selected_commands(self) -> List[Command]:
        selected_commands = []
        for command, check_box in self.command_check_boxes:
            if check_box.isChecked():
                selected_commands.append(command)
        return selected_commands

    def _load_profile(self, profile: Profile) -> None:
        commands_included = list(profile.get_commands())
        for command, check_box in self.command_check_boxes:
            if command in commands_included:
                check_box.setCheckState(QtCore.Qt.Checked)
            else:
                check_box.setCheckState(QtCore.Qt.Unchecked)

    def _save_profile(self) -> None:
        profile_id = config["current_profile_id"]
        for command, check_box in self.command_check_boxes:
            profile_command = ProfileCommand.get_or_none(
                profile=profile_id, command=command.id
            )
            if check_box.isChecked():
                if profile_command is None:
                    ProfileCommand(profile=profile_id, command=command.id).save()
            else:
                if profile_command is not None:
                    profile_command.delete_instance()
