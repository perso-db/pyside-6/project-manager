# -*- coding: utf-8 -*-

"""
Defines :
 The CookiecutterLaunchWidget class.

"""

from __future__ import annotations

from typing import List, Tuple, Optional

from PySide6 import QtWidgets
from donb_custom_widget.my_custom_widget import MyCustomWidget

from donb_project_manager.config import config
from donb_project_manager import cookiecutter_template


class CookiecutterLaunchWidget(QtWidgets.QWidget, MyCustomWidget):

    """

    Warning
    -------
    This widget should not be instantiated directly, but rather through the factory
    method create_cookiecutter_launch_widget
    """

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None):
        super().__init__(parent)
        self.template: cookiecutter_template.CookiecutterTemplate
        self.parameters: List[Tuple[str, QtWidgets.QLineEdit]] = []

    @classmethod
    def create_cookiecutter_launch_widget(
        cls, template: cookiecutter_template.CookiecutterTemplate
    ) -> CookiecutterLaunchWidget:
        """Factory method to create a CookiecutterLaunchWidget."""
        # create_cookiecutter_launch_widget is a factory method, and should therefore
        # be allowed to access protected members of the class.
        # pylint: disable = protected-access
        cookiecutter_launch_widget = cls.create_widget()
        assert isinstance(cookiecutter_launch_widget, cls)
        cookiecutter_launch_widget.template = template
        cookiecutter_launch_widget.template_name_label.setText(template.name)
        cookiecutter_launch_widget._setup_ui()
        cookiecutter_launch_widget._connect_actions()
        return cookiecutter_launch_widget

    def _setup_ui(self) -> None:
        self.working_dir_line_edit.setText(str(config["projects_dir"].absolute()))
        for index, key in enumerate(self.template.get_parameters()):
            label = QtWidgets.QLabel(self)
            label.setText(key)
            label.setMinimumWidth(120)
            self.parameters_grid_layout.addWidget(label, index, 0)
            line_edit = QtWidgets.QLineEdit(self)
            line_edit.setText(self.template.json[key])
            self.parameters_grid_layout.addWidget(line_edit, index, 1)
            self.parameters.append((key, line_edit))

    def _connect_actions(self) -> None:
        self.ok_button.clicked.connect(self._handle_ok)
        self.cancel_button.clicked.connect(self.close)

    def _handle_ok(self) -> None:
        extra_context = {key: line_edit.text() for key, line_edit in self.parameters}
        self.template.create(extra_context, self.working_dir_line_edit.text())
        self.close()
