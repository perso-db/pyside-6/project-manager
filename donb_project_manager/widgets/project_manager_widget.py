# -*- coding: utf-8 -*-

"""
Defines :
 The ProjectManagerWidget class.

"""


from __future__ import annotations

from pathlib import Path
from typing import Optional

from PySide6 import QtWidgets
from donb_custom_widget.my_custom_widget import MyCustomWidget

from donb_project_manager.config import config
from donb_project_manager.models import Project
from donb_project_manager.widgets import my_main_window as main_window


class ProjectManagerWidget(QtWidgets.QWidget, MyCustomWidget):

    """
    The ProjectManagerWidget allows to edit and save parameters relative to a Project.
    """

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None) -> None:
        super().__init__(parent)
        self.project: Project
        self.my_main_window: main_window.MyMainWindow

    @classmethod
    def create_project_manager_widget(
        cls, project: Project, my_main_window: main_window.MyMainWindow
    ) -> ProjectManagerWidget:
        """
        Factory method to create an ExtractsWidget.

        Parameters
        ----------
        project
            The project being edited.
        my_main_window
            The main window of the application, which will need to be refreshed if the
            project's values change.

        """
        # create_project_manager_widget is a factory method, and should therefore be
        # allowed to access protected members of the class.
        # pylint: disable = protected-access
        project_manager_widget = cls.create_widget()
        assert isinstance(project_manager_widget, cls)
        project_manager_widget.project = project
        project_manager_widget.my_main_window = my_main_window
        project_manager_widget._init_values()
        project_manager_widget._connect_actions()
        return project_manager_widget

    def _init_values(self) -> None:
        self.working_dir_line_edit.setText(self.project.working_dir)
        self.title_line_edit.setText(self.project.title)
        self.repo_name_line_edit.setText(self.project.repo_name)
        self.conda_env_line_edit.setText(self.project.conda_env)
        self.version_line_edit.setText(self.project.version)

    def _connect_actions(self) -> None:
        self.working_dir_button.clicked.connect(self._select_working_dir)
        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self._handle_ok)
        self.version_button.clicked.connect(self._change_version)

    def _select_working_dir(self) -> None:
        working_dir = QtWidgets.QFileDialog.getExistingDirectory(
            parent=None,
            caption="Select the working directory for the project",
            dir=str(config['projects_dir']),
        )
        self.working_dir_line_edit.setText(working_dir)

    def _handle_ok(self) -> None:
        self._save_project()
        config['current_project_id'] = self.project.id
        config.save()
        self.my_main_window.refresh()
        self.close()

    def _save_project(self) -> None:
        self.project.title = self.title_line_edit.text()
        self.project.repo_name = self.repo_name_line_edit.text()
        self.project.conda_env = self.conda_env_line_edit.text()
        self.project.working_dir = self.working_dir_line_edit.text()
        self.project.version = self.version_line_edit.text()
        self.project.save()

    def _change_version(self) -> None:
        setup_file = Path(self.project.working_dir) / "setup.py"
        self._modify_file(
            setup_file, "version=", f'version="{self.version_line_edit.text()}",\n'
        )
        conf_file = Path(self.project.working_dir) / "conf.py"
        self._modify_file(
            conf_file, "release =", f'release = "{self.version_line_edit.text()}"\n'
        )

    @staticmethod
    def _modify_file(file_path: Path, search_tag: str, new_line: str) -> None:
        with open(file_path, "r") as old_file:
            old_content = old_file.read()
        content_before_version = old_content.split(search_tag)[0]
        content_after_version = "\n".join(
            old_content.split(search_tag)[1].split("\n")[1:]
        )
        new_content = content_before_version + new_line + content_after_version
        with open(file_path, "w") as new_file:
            new_file.write(new_content)
