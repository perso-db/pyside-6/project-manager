# -*- coding: utf-8 -*-

"""
Defines :
 The SelectProjectWidget class.

"""

from __future__ import annotations

from pathlib import Path
from typing import Optional, Tuple, List, Union

from PySide6 import QtWidgets
from donb_custom_widget.my_custom_widget import MyCustomWidget

from donb_project_manager.config import config
from donb_project_manager.models import Project
from donb_project_manager.scripts import restore_project
from donb_project_manager.widgets import my_main_window as main_window


class SelectProjectWidget(QtWidgets.QWidget, MyCustomWidget):

    """
    The SelectProjectWidget allows to select a project from the list of
    the existing ones.
    """

    def __init__(self, parent: Optional[QtWidgets.QWidget] = None):
        super().__init__(parent)
        self.my_main_window: main_window.MyMainWindow
        self.project_radio_button: List[Tuple[Project, QtWidgets.QRadioButton]] = []

    @classmethod
    def create_select_project_widget(
        cls, my_main_window: main_window.MyMainWindow
    ) -> SelectProjectWidget:
        """Factory method to create an ExtractsWidget."""
        # create_project_manager_widget is a factory method, and should therefore be
        # allowed to access protected members of the class.
        # pylint: disable = protected-access
        select_project_widget = cls.create_widget()
        assert isinstance(select_project_widget, cls)
        select_project_widget.my_main_window = my_main_window
        select_project_widget._connect_actions()
        return select_project_widget

    def _connect_actions(self) -> None:
        self.cancel_button.clicked.connect(self.close)
        self.ok_button.clicked.connect(self._handle_ok)

    def _handle_ok(self) -> None:
        raise NotImplementedError()

    def _get_selected_project(self) -> Union[Path, Project]:
        for value, radio_button in self.project_radio_button:
            if radio_button.isChecked():
                return value
        raise ValueError("Should never happen...")


class OpenProjectWidget(SelectProjectWidget):
    @classmethod
    def create_open_project_widget(
        cls, my_main_window: main_window.MyMainWindow
    ) -> OpenProjectWidget:
        """Factory method to create an ExtractsWidget."""
        # create_project_manager_widget is a factory method, and should therefore be
        # allowed to access protected members of the class.
        # pylint: disable=protected-access
        open_project_widget = SelectProjectWidget.create_select_project_widget(
            my_main_window
        )
        open_project_widget.__class__ = OpenProjectWidget
        assert isinstance(open_project_widget, OpenProjectWidget)
        open_project_widget._init_values()
        return open_project_widget

    def _init_values(self) -> None:
        current_project = Project.get(id=config["current_project_id"])
        for project in Project.select().order_by(Project.title):
            new_button = QtWidgets.QRadioButton(project.title, self)
            if current_project == project:
                new_button.setChecked(True)
            self.projects_layout.addWidget(new_button)
            self.project_radio_button.append((project, new_button))

    def _get_selected_project(self) -> Project:
        # Function redefined only to update signature
        selected_project = super()._get_selected_project()
        assert isinstance(selected_project, Project)
        return selected_project

    def _handle_ok(self) -> None:
        selected_project = self._get_selected_project()
        config["current_project_id"] = selected_project.id
        config.save()
        self.my_main_window.refresh()
        self.close()


class RestoreProjectWidget(SelectProjectWidget):
    @classmethod
    def create_restore_project_widget(
        cls, my_main_window: main_window.MyMainWindow
    ) -> RestoreProjectWidget:
        """Factory method to create an ExtractsWidget."""
        # create_project_manager_widget is a factory method, and should therefore be
        # allowed to access protected members of the class.
        # pylint: disable = protected-access
        restore_project_widget = SelectProjectWidget.create_select_project_widget(
            my_main_window
        )
        restore_project_widget.__class__ = RestoreProjectWidget
        assert isinstance(restore_project_widget, RestoreProjectWidget)
        restore_project_widget._init_values()
        return restore_project_widget

    def _init_values(self) -> None:
        for file in config["archive_dir"].iterdir():
            if file.suffix == ".zip":
                new_button = QtWidgets.QRadioButton(file.stem, self)
                self.projects_layout.addWidget(new_button)
                self.project_radio_button.append((file, new_button))

    def _get_selected_project(self) -> Path:
        # Function redefined only to update signature
        selected_project = super()._get_selected_project()
        assert isinstance(selected_project, Path)
        return selected_project

    def _handle_ok(self) -> None:
        selected_project = self._get_selected_project()
        restore_project(selected_project)
        self.my_main_window.refresh()
        self.close()
