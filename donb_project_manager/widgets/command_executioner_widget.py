# -*- coding: utf-8 -*-

"""
Defines :
 The CommandExecutionerWidget class.

"""

from __future__ import annotations

import re
import subprocess
from typing import List, Optional

from PySide6 import QtWidgets, QtCore
from donb_custom_widget.my_custom_widget import MyCustomWidget

from donb_project_manager.config import config
from donb_project_manager.models import Command, get_current_project
from donb_project_manager import scripts
from donb_project_manager.widgets import my_main_window


class CommandExecutionerWidget(QtWidgets.QWidget, MyCustomWidget):

    """
    The CommandExecutionerWidget displays the list of commands being executed
    with their status. The output of the command is displayed in the terminal.

    Warning
    -------
    This widget should not be instantiated directly, but rather through the factory
    method create_command_executioner_widget

    """

    def __init__(self, parent: Optional[QtWidgets.QWidget]):
        super().__init__(parent)
        self.commands: List[Command]
        self.my_thread: MyThread
        self.command_formater: CommandFormatter
        self.my_main_window: my_main_window.MyMainWindow

    @classmethod
    def create_command_executioner_widget(
        cls, commands: List[Command], main_window: my_main_window.MyMainWindow
    ) -> CommandExecutionerWidget:
        """
        Factory method to create a CommandExecutionerWidget.

        Parameters
        ----------
        commands
            The list of commands to be executed. These commands can contain tags
            of the type {{project.var_name}}, which will be replaced by the
            project's corresponding value before the execution.
        project
            The project onto which the commands will be executed.

        """
        # create_command_executioner_widget is a factory method, and should therefore
        # be allowed to access protected members of the class.
        # pylint: disable = protected-access
        command_executioner_widget = cls.create_widget(None)
        assert isinstance(command_executioner_widget, cls)
        command_executioner_widget.my_main_window = main_window
        command_executioner_widget.commands_text_edit.setStyleSheet(
            'font: 11pt "Fira Code"; color: white'
        )
        command_executioner_widget.commands = commands
        return command_executioner_widget

    def add_text(self, new_line: str) -> None:
        """Adds a line of text to the main TextEdit widget."""
        if (to_insert := new_line.strip()) != "":
            self.commands_text_edit.append(to_insert)

    def execute(self) -> None:
        """Starts the execution of the commands in a separate thread."""
        command_lines = self._get_command_lines()
        self.command_formater = CommandFormatter(self.commands_text_edit, command_lines)
        self.command_formater.refresh()
        self.my_thread = MyThread(command_lines)
        self.my_thread.running_cmd.connect(  # type: ignore  # pylint: disable=no-member
            self.command_formater.running_cmd
        )
        self.my_thread.finished_cmd.connect(  # type: ignore  # pylint: disable=no-member
            self.command_formater.finished_cmd
        )
        self.my_thread.finished_all.connect(  # type: ignore  # pylint: disable=no-member
            self.command_formater.finished_all
        )
        self.my_thread.finished.connect(  # type: ignore  # pylint: disable=no-member
            self.my_main_window.refresh
        )
        self.my_thread.start()

    def _get_command_lines(self) -> List[str]:
        command_lines = []
        current_project = get_current_project()
        for command in self.commands:
            new_command_line = command.command_line
            if command.need_conda_env:
                new_command_line = (
                    f"{config['conda_script_path']} & "
                    f"conda activate {current_project.conda_env} & "
                    f"{new_command_line}"
                )
            for command_part in new_command_line.split("|"):
                transformed_command = self._transform_command_line(command_part)
                command_lines.append(transformed_command)
        return command_lines

    @staticmethod
    def _transform_command_line(command_part: str) -> str:
        current_project = get_current_project()
        my_regex = r"{{project\.(.*?)}}"
        matchs = re.findall(my_regex, command_part)
        new_command_line = command_part
        for attribute in matchs:
            old_text = f"{{{{project.{attribute}}}}}"
            new_text = getattr(current_project, attribute)
            new_command_line = new_command_line.replace(old_text, new_text)
        my_regex = r"{{config\.(.*?)}}"
        matchs = re.findall(my_regex, new_command_line)
        for attribute in matchs:
            old_text = f"{{{{config.{attribute}}}}}"
            new_text = str(config[attribute])
            new_command_line = new_command_line.replace(old_text, new_text)
        return new_command_line


class MyThread(QtCore.QThread):
    """
    A thread used to actually run the commands.

    Parameters
    ----------
    command_lines
        The list of command lines to be executed. These commands can contain tags
        of the type {{project.var_name}}, which will be replaced by the
        project's corresponding value before the execution.

    """

    running_cmd: QtCore.Signal = QtCore.Signal(str)
    finished_cmd: QtCore.Signal = QtCore.Signal(str, int)
    finished_all: QtCore.Signal = QtCore.Signal()

    def __init__(self, command_lines: List[str]):
        super().__init__()
        self.command_lines: List[str] = command_lines
        self.working_dir: str = get_current_project().working_dir

    def run(self) -> None:
        """Executes the commands and signals their state to the widget."""
        for command in self.command_lines:
            if command.startswith("script:"):
                self.run_script(command)
            else:
                self.run_console(command)
        self.finished_all.emit()  # type: ignore  # pylint: disable = no-member

    def run_console(self, command: str) -> None:
        self.running_cmd.emit(command)  # type: ignore  # pylint: disable = no-member
        result = subprocess.run(command, cwd=self.working_dir, shell=True, check=False)
        self.finished_cmd.emit(  # type: ignore  # pylint: disable = no-member
            command, result.returncode
        )

    def run_script(self, command: str) -> None:
        script_command = command[len("script:"):]
        script_name = script_command.split(",")[0]
        script_args = script_command.split(",")[1:]
        script = getattr(scripts, script_name)
        result = script(*script_args)
        self.finished_cmd.emit(  # type: ignore  # pylint: disable = no-member
            command, result
        )


class CommandFormatter:
    """
    Formats the command lines before displaying them in a QTextEdit.

    Parameters
    ----------
    text_edit
        The QTextEdit in which the command lines will be displayed.
    command_lines
        The command lines to display.

    """

    def __init__(self, text_edit: QtWidgets.QTextEdit, command_lines: List[str]):
        self.text_edit = text_edit
        self.command_lines = command_lines
        self.state = {command_line: 0 for command_line in self.command_lines}

    def format(self) -> str:
        """Returns the text to be displayed in the QTextEdit."""
        current_project = get_current_project()
        command_lines_formatted = "Répertoire de travail :\n"
        command_lines_formatted += current_project.working_dir + "\n\n"
        for command_line, state in self.state.items():
            if command_line.startswith("C:/Users/CassanR/Miniconda3/Scripts/activate"):
                command_line = "".join(command_line.split(" & ")[2:])
            if state == 0:
                command_lines_formatted += command_line + "\n"
            if state == 1:
                command_lines_formatted += command_line + " : en cours" + "\n"
            if state == 2:
                command_lines_formatted += command_line + " : fini OK" + "\n"
            if state == 3:
                command_lines_formatted += command_line + " : fini NOK" + "\n"
        return command_lines_formatted

    def refresh(self) -> None:
        """Refreshes the text displayed in the QTextEdit."""
        self.text_edit.setText(self.format())

    def running_cmd(self, command_line: str) -> None:
        """Handles the fact that a new command is running."""
        self.state[command_line] = 1
        self.refresh()

    def finished_cmd(self, command_line: str, return_code: int) -> None:
        """Handles the fact that a command has finished."""
        self.state[command_line] = 2 if return_code == 0 else 3
        self.refresh()

    def finished_all(self) -> None:
        """Handles the fact that all commands have finished."""
        self.text_edit.append("Done !!")
