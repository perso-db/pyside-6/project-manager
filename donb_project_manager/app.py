#!  project_manager
# -*- coding: utf-8 -*-
"""
Launches the app.
"""


import sys
from pathlib import Path

# my import style (adding "donb_project_manager" everywhere to make it all
# more explicit) makes needed to have the script's parent folder added to system
# path before importing local modules.
# pylint: disable=wrong-import-position

my_package_path = Path(__file__).parent.parent
sys.path.insert(0, str(my_package_path.absolute()))

from PySide6 import QtWidgets, QtCore, QtGui
from donb_tools.functions import get_data_folder

from donb_project_manager.widgets.my_main_window import MyMainWindow


def main() -> None:
    """Creates and launches the app."""
    app = QtWidgets.QApplication(sys.argv)
    _apply_style_sheet(app)
    _apply_icon(app)
    my_main_window = MyMainWindow.create_my_main_window()
    my_main_window.show()
    sys.exit(app.exec_())


def _apply_icon(app: QtWidgets.QApplication) -> None:
    icon_path = get_data_folder() / "images" / "main_icon.jpg"
    icon = QtGui.QIcon(str(icon_path))
    app.setWindowIcon(icon)


def _apply_style_sheet(app: QtWidgets.QApplication) -> None:
    data_folder = get_data_folder()
    style_sheet_file = data_folder / "style_sheet.css"
    with open(style_sheet_file, "r") as style_sheet:
        style_sheet_content = style_sheet.read()
    icon_folder = data_folder / "theme"
    QtCore.QDir.addSearchPath("icon", str(icon_folder))
    app.setStyleSheet(style_sheet_content)


if __name__ == "__main__":
    main()
