# -*- coding: utf-8 -*-

"""
Defines :
 The CookiecutterTemplate class.

"""


import json
import os
from pathlib import Path
from typing import Dict, List

from cookiecutter.main import cookiecutter

from donb_project_manager.models import get_current_project
from donb_project_manager.widgets.cookiecutter_launch_widget import (
    CookiecutterLaunchWidget,
)


class CookiecutterTemplate:

    """
    A cookiecutter template.
    """

    def __init__(self, template_path: Path):
        self.path = template_path
        self.name = self.path.name
        self.json: Dict[str, str] = {}
        self._analyse_json()

    def _analyse_json(self) -> None:
        with open(self.path / "cookiecutter.json") as json_file:
            data_raw = json_file.read()
        data_json = json.loads(data_raw)
        self.json = data_json

    def _get_value_from_project(self) -> None:
        current_project = get_current_project()
        for key in self.json:
            try:
                project_value = getattr(current_project, key)
            except AttributeError:
                pass
            else:
                self.json[key] = project_value

    def get_parameters(self) -> List[str]:
        """The list of parameters defined in the cookiecutter template json."""
        return [key for key in self.json.keys() if not key.startswith("_")]

    def show_widget(self) -> None:
        """Launches the CookieCutter widget."""
        self._get_value_from_project()
        # For some reason, Black wants to reformat the line below with a line 96
        # characters long.
        # fmt: off
        cookiecutter_launch_widget = (
            CookiecutterLaunchWidget.create_cookiecutter_launch_widget(self)
        )
        # fmt: on
        cookiecutter_launch_widget.show()

    def create(self, extra_context: Dict[str, str], working_dir: str) -> None:
        """
        Creates the project based on the cookiecutter template.

        Parameters
        ----------
        extra_context
            A dict holding the values corresponding to the template's parameters.
        working_dir
            The directory where the project will be created.
        """
        old_dir = os.getcwd()
        new_dir = Path(working_dir)
        os.chdir(new_dir)
        cookiecutter(
            str(self.path.absolute()), extra_context=extra_context, no_input=True,
        )
        os.chdir(old_dir)
