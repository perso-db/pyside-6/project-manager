# -*- coding: utf-8 -*-

"""
Defines the Peewee models used in the application:
 - The base class ProjectManagerBaseClass

 -  The derived classes Project

Also defines a convenience function to get the current Project associated to the
main window : get_current_projet

"""

from __future__ import annotations

from typing import Iterable

from donb_config.config_database import ConfigDatabase
from peewee import (
    BooleanField,
    Model,
    ForeignKeyField,
    CharField,
    AutoField,
)

from donb_project_manager.config import config


class ProjectManagerBaseModel(Model):
    """Base class, only used to set the database."""

    class Meta:  # pylint: disable=missing-class-docstring
        assert isinstance(config, ConfigDatabase)
        database = config.database


class Project(ProjectManagerBaseModel):
    """
    A Project holds all information relative to a project.

    Attributes
    ----------
    title
        The name of the project, used only to display information. Can contains spaces
        and any kind of character.
    repo_name
        Used for : the git repo name, the pip package name, the conda package name. It
        also nees to be the name of the dir holding the sources.
    conda_env
        The name of the conda environment used for the project.
    working_dir
        The path to the working directory.
    version
        The current version of the project.

    """

    id: int = AutoField(primary_key=True, column_name="id")
    title: str = CharField()
    repo_name: str = CharField()
    conda_env: str = CharField()
    working_dir: str = CharField()
    version: str = CharField()


class Command(ProjectManagerBaseModel):
    """
    A command to be executed.

    Attributes
    ----------
    label
        The name used to identify the command in the GUI.
    layout_name
        The layout into which to insert the button for the command.
    command_line
        The actual command line. Can contains tag ( {{project.my_var}} ).
    need_conda_env
        Whether or not the command need to be executed in the conda environment of
        the project.

    """

    id: int = AutoField(primary_key=True, column_name="id")
    label: str = CharField()
    layout_name: str = CharField()
    command_line: str = CharField()
    need_conda_env: bool = BooleanField()


class Profile(ProjectManagerBaseModel):
    """
    A profile is a container holding a specific set of commands.

    Attributes
    ----------
    name
        The name of the profile.

    """

    id: int = AutoField(primary_key=True, column_name="id")
    name: str = CharField()

    def get_commands(self) -> Iterable[Command]:
        """The list of commands associated with the Profile."""
        commands: Iterable[Command] = (
            Command.select()
            .join(ProfileCommand)
            .join(Profile)
            .where(Profile.id == self.id)
        )
        return commands


class ProfileCommand(ProjectManagerBaseModel):

    """
    Technical command .

    Attributes
    ----------
    name
        The name of the profile.

    """

    id: int = AutoField(primary_key=True, column_name="id")
    profile: Profile = ForeignKeyField(Profile, column_name="profile")
    command: Command = ForeignKeyField(Command, column_name="command")

    class Meta:  # pylint: disable=missing-class-docstring
        table_name = "profile_command"


def get_current_project() -> Project:
    """Returns the Project object currently associated with the main window."""
    current_project: Project = Project.get(id=config["current_project_id"])
    return current_project
