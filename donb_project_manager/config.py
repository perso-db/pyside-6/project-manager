# -*- coding: utf-8 -*-

"""
Defines :
 One config instance, to be imported in all modules that might need it.

"""

from donb_config.config import Config
from donb_tools.functions import get_data_folder

_ini_file = get_data_folder() / "config.ini"
config: Config = Config.create(_ini_file)
