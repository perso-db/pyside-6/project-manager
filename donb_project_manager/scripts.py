# -*- coding: utf-8 -*-

"""
Defines scripts usable in the commands.
"""
import json
import pickle
import shutil
from pathlib import Path
from zipfile import ZipFile

from donb_project_manager.config import config
from donb_project_manager.models import Project, get_current_project


def _create_zip(project: Project, dpbm_file_path: Path) -> None:
    zip_file_path: Path = config['archive_dir'] / f"{project.title}.zip"
    zip_file_path.unlink(missing_ok=True)
    working_dir = Path(project.working_dir)
    with ZipFile(zip_file_path, 'x') as zip_file:
        for element in working_dir.rglob("*"):
            if element.suffix == ".dbpm":
                zip_file.write(dpbm_file_path, dpbm_file_path.relative_to(working_dir))
            else:
                zip_file.write(element, element.relative_to(working_dir.parent))


def _create_dbpm_file(project: Project) -> Path:
    dbpm_file_path = Path(project.working_dir) / f"{project.title}.dbpm"
    with open(dbpm_file_path, 'wb') as dbpm_file:
        pickle.dump(project, dbpm_file)
    return dbpm_file_path


def archive_current_project() -> int:
    current_project = get_current_project()
    dpbm_file_path = _create_dbpm_file(current_project)
    _create_zip(current_project, dpbm_file_path)
    shutil.rmtree(current_project.working_dir)
    current_project.delete_instance()
    config['current_project_id'] = Project.get().id
    config.save()
    return 0


def restore_project(zip_file_path: Path) -> None:
    project = _get_project_from_archive(zip_file_path)
    project = _restore_project_model(project)
    _unzip_archive(project, zip_file_path)
    config['current_project_id'] = project.id
    config.save()


def _restore_project_model(project):
    new_project = Project.get_or_none(title=project.title)
    if new_project is None:
        project.id = None
        project.save(force_insert=True)
    else:
        project = new_project
    new_working_dir = config['projects_dir'] / project.working_dir.split("/")[-1]
    project.working_dir = new_working_dir
    project.save()
    return project


def _unzip_archive(project, zip_file_path):
    if project.working_dir.exists():
        print("already existing, no unzipping file")
    else:
        with ZipFile(zip_file_path, 'r') as zip_file:
            zip_file.extractall(path=project.working_dir.parent)
        dbpm_file_path = config['projects_dir'] / (project.title + ".dbpm")
        dbpm_file_path.unlink()


def _get_project_from_archive(zip_file_path):
    with ZipFile(zip_file_path, 'r') as zip_file:
        for filename in zip_file.namelist():
            if filename.endswith(".dbpm"):
                with zip_file.open(filename) as dbpm_file:
                    project = pickle.load(dbpm_file)
                    return project
    return None
