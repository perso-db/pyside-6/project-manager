# -*- mode: python
# coding: utf-8 -*-

block_cipher = None

a = Analysis(['donb_project_manager\\app.py'],
             pathex=['.'],
             binaries=[],
             datas=[('.\\data', 'data'),
                    ('.\\documentation', 'documentation'),
                    ('C:\\Users\\CassanR\\Miniconda3\\envs\\project_manager\\Lib\\site-packages\\PySide6\\plugins', 'PySide6\\plugins'),
                    ],
             hiddenimports=['PySide6.QtXml'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
			
			 
pyz = PYZ(a.pure,
          a.zipped_data,
		  cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='Project Manager',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
		  icon='.\\data\\images\\main_icon.ico')
		  
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='Project Manager')
